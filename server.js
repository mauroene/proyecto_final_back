//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3002;

var requestjson = require('request-json');
var path = require('path');

var bodyparser = require('body-parser');
  app.use(bodyparser.json());

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

// Constantes para todas las Consultas
var urlRaiz =  "https://api.mlab.com/api/1/databases/mnajera/collections";
const apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"

// Constantes para bcrypt
const bcrypt = require('bcrypt');
const saltRounds = 10;

// Constantes para las Condiciones del Clima
const urlRaizOpenWather =  "http://api.openweathermap.org/data/2.5/weather"
const appId = "c3afdfff7c1320914c4dd6f582441f94";
const units =  "metric";
const lng =  "es";

app.listen(port);

console.log('Proyecto Final (BACK)(Curso Practicioner 4Ed): Cotizador Seguro de Salud en puerto: ' + port);

// CONSULTA de Condiciones del Clima
app.get('/v1/Clima/:cityId', function(req,res) {
  //console.log(req.params.cityId);
  var urlOpenWather =  urlRaizOpenWather + "?" + "APPID=" + appId + "&units=metric&lang=es" + "&id=" + req.params.cityId;
  // console.log(urlOpenWather);

  var clima = requestjson.createClient(urlOpenWather);

  clima.get('', function(err, resM, body){
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

// CONSULTA de Cuotas de Tarifas
app.get('/v1/Tarifas/:clave', function(req,res) {
  var query = "&q={clave:" + '"' + req.params.clave + '"}';
  var urlConsulta =  urlRaiz + "/Tarifas?" + apiKey + query
  //  console.log(urlConsulta);
  var tarifaDBMongo = requestjson.createClient(urlConsulta);

  tarifaDBMongo.get('', function(err, resM, body){
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

// CONSULTA de Usuario
app.post('/v1/login', function(req, res) {
  var email = req.body.email
  var password = req.body.password
  var passwordBD;
  var validacion;

  // Login del Usuario

  // Consulta del usuario para validar si existe
  var query = '&q={"email":"'+ email +'"}' + '&c=true';
  var urlConsulta =  urlRaiz + "/Usuarios?" + apiKey + query;
  //console.log(urlConsulta);
  var clienteDBMongo = requestjson.createClient(urlConsulta);
  clienteDBMongo.get('',function(err, resM, body) {
      if (!err) {
        if (body == 1) {
          // El usuario existe:
          query = '&q={"email":"'+ email +'"}';
          urlConsulta =  urlRaiz + "/Usuarios?" + apiKey + query
          //console.log(urlConsulta);
          clienteDBMongo = requestjson.createClient(urlConsulta);
          clienteDBMongo.get('',function(err, resM, body) {
              if (!err) {
                console.log('Usuario existente');
                //  AQUI FALTARIA SABER EL COUNT() PARA DECIR SI ESTA REPETIDO O ES UNICO
                passwordBD =  body[0]["password"];
                // console.log('Password BD: ' + passwordBD);
                // a) Se genera salt (cadena aleatoria);
                // b) Se genera el hash para el password capturado por Usuario;
                // c) Se valida el password de la BD vs el passsword encriptado
                var start = Date.now();
                bcrypt.genSalt(saltRounds, function(err, salt) {
                  // console.log('salt: ' + salt);
                  // console.log('salt cb end: ' + (Date.now() - start) + 'ms');
                  bcrypt.hash(password, salt, function(err, crypted) {
                    // console.log('crypted: ' + crypted);
                    // console.log('crypted cb end: ' + (Date.now() - start) + 'ms');
                    // console.log('rounds used from hash:', bcrypt.getRounds(crypted));
                    bcrypt.compare(password, passwordBD, function(err, res2) {
                      validacion =  res2;
                      // console.log('compared true: ' + res2);
                      // console.log('compared true cb end: ' + (Date.now() - start) + 'ms');
                      if (validacion == true) {
                        console.log('Password del usuario coincide');
                        res.status(200).send('Password del usuario coincide');
                      } else {
                        console.log('Password del usuario NO coincide');
                        res.status(404).send('Password del usuario NO coincide');
                      }
                      //  console.log('end: ' + (Date.now() - start) + 'ms');
                    });
                  });
                })
              } else {
                // Error al consultar un cliente existente
                console.log('Error al consultar un cliente existente');
                console.log(body);
              }
            })
        } else {
          // El usuario no existe
          console.log('Usuario NO existente, registrese');
          res.status(404).send('Usuario NO existente, registrese');
        }
      } else {
        // Error al validar si el cliente existe
        console.log('Error al validar si el cliente existe');
        console.log(body);
      }
    })
})

// ALTA de Usuario
app.post('/v1/registro', function(req, res) {
  //console.log(req.body);
  var password = req.body.password
  passwordBD = password;

  // Encripta el Password:
  bcrypt.genSalt(saltRounds, function(err, salt) {
    //console.log("Salt: " + salt);
    bcrypt.hash(password, salt, function(err, hash) {
      // Alta del Usuario
      //console.log("Hash: " + hash);
      req.body.password = hash;

      var query = '&q={"email":"'+ req.body.email +'"}' + '&c=true';
      var urlActualiza =  urlRaiz + "/Usuarios?" + apiKey
      //console.log(urlActualiza);

      var clienteDBMongo = requestjson.createClient(urlActualiza);
      clienteDBMongo.post('',req.body,function(err, resM, body) {
        if (err) {
        console.log('Hay error en alta de Cliente');
        console.log(body);
        } else {
        console.log('No hay error en alta de Cliente');
        res.send(body);
        }
      })
    });
  });
})
